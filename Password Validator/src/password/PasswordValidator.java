package password;

/*
 * 
 * @author Andy Tran - 991551797
 * 
 * Validates passwords and is being developed using TDD
 * 
 * */

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	
	/**
	 * 
	 * @param pw
	 * @return true if the number of characters is 8 or more. No spaces allowed.
	 */
	public static boolean isValidLength(String pw) {
		
		return pw.indexOf(" ") < 0 && pw.length() >= MIN_LENGTH;
		
//		if(pw.indexOf(" ") >= 0) {
//			return false;
//		}
//		return pw.length() >= MIN_LENGTH;
	}
	
	public static boolean hasDoubleDigits(String pw) {
		
		int cnt = 0;
		
		for (int i = 0; i < pw.length(); i++) {
			if (Character.isDigit(pw.charAt(i))) {
				cnt++;
			}
		}
//		System.out.println(cnt);
		return cnt >= 2;
	}
	
}
