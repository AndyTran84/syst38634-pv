package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * 
 * @author Andy Tran - 991551797
 * 
 * */

public class PasswordValidatorTest {
	
	@Test
	public void testHasDoubleDigitsBout() {
		boolean results = PasswordValidator.hasDoubleDigits("AndyTran1");
		assertFalse("Invalid digits", results);
	}
	
	@Test
	public void testHasDoubleDigitsBin() {
		boolean results = PasswordValidator.hasDoubleDigits("AndyTran1209");
		assertTrue("Has less than 2 digits", results);
	}
	
	@Test
	public void testHasDoubleDigitsException() {
		boolean results = PasswordValidator.hasDoubleDigits("AndyTran");
		assertFalse("Has more than 2 digits", results);
	}
	
	@Test
	public void testHasDoubleDigitsRegular() {
		boolean results = PasswordValidator.hasDoubleDigits("AndyTran12099");
		assertTrue("Has less than 2 digits", results);
	}
	
	@Test
	public void testIsValidLengthBout() {
		boolean results = PasswordValidator.isValidLength("12345");
		assertFalse("Password is correct length", results);
	}
	
	@Test
	public void testIsValidLengthRegular() {
		boolean results = PasswordValidator.isValidLength("AndyTran1209");
		assertTrue("Password not long enough", results);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean results = PasswordValidator.isValidLength("Andy9");
		assertFalse("Password is correct length", results);
	}

	
	@Test
	public void testIsValidLengthBin() {
		boolean results = PasswordValidator.isValidLength("12345678");
		assertTrue("Password is not correct length", results);
	}
	
	
//	// tests if there are spaces on either side of the pw
//	@Test
//	public void testIsValidLengthExceptionSpaces() {
//		boolean results = PasswordValidator.isValidLength("    Andy   ");
//		assertFalse("Password is correct length", results);
//	}

}
